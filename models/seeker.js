const mongoose = require("mongoose");

//Create Seeker Schema
let SeekerSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  org: {
    type: String
  },
  email: {
    type: String,
    required: true
  }
});

module.exports = Seeker = mongoose.model("seeker", SeekerSchema);
