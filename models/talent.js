const mongoose = require("mongoose");

let TalentSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  dob: {
    type: Date
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String
  },
  phone_number: {
    type: String
  },
  role: {
    type: Number, // 0 for talent 1 for recruiter,
    default: 0,
    required: true
  }
});

let TalentModel = mongoose.model("talent", TalentSchema);

module.exports = TalentModel;
