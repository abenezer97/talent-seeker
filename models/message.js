const mongoose = require("mongoose")

let MessageSchema = new mongoose.Schema({
    seeker: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'talent'
    },
    talent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'talent'
    },
    messages: [{
        body: {
            type: String
        },
        sender: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'talent'
        },
        sent_date: {
            type: Date,
            default: Date.now
        }
    }]
})

let MessageModel = mongoose.model("message", MessageSchema)
module.exports = MessageModel