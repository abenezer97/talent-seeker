const mongoose = require("mongoose")

let InterviewSchema = new mongoose.Schema({
    seeker: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'talent'
    },
    talent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'talent'
    },
    interview_date: {
        type: Date
    },
    interview_accepted: {
        type: Boolean,
        default: false
    }
})

let InterviewModel = mongoose.model('interview', InterviewSchema)
module.exports = InterviewModel