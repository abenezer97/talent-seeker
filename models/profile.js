const mongoose = require("mongoose");

let ProfileSchema = new mongoose.Schema({
  talent: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "talent"
  },
  bio: {
    type: String,
    required: true
  },
  website: {
    type: String
  },
  location: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  githubusername: {
    type: String
  },
  skills: [
    {
      skill: {
        type: String
      },
      level: {
        type: String
      },
      experience: {
        type: String
      }
    }
  ],
  experience: [
    {
      title: {
        type: String,
        required: true
      },
      company: {
        type: String,
        required: true
      },
      location: {
        type: String
      },
      from: {
        type: Date,
        required: true
      },
      to: {
        type: Date
      },
      current: {
        type: Boolean,
        default: false
      }
    }
  ],
  projects: [
    {
      project_name: {
        type: String
      },
      description: {
        type: String
      },
      link: {
        type: String
      }
    }
  ],
  education: [
    {
      school: {
        type: String,
        required: true
      },
      degree: {
        type: String,
        required: true
      },
      fieldofstudy: {
        type: String
      },
      from: {
        type: Date,
        required: true
      },
      to: {
        type: Date
      },
      current: {
        type: Boolean,
        default: false
      }
    }
  ],
  availability: {
    available: {
      type: Boolean,
      default: true
    },
    availablity_date: {
      type: Date,
      default: null
    }
  },
  trainings: [
    {
      type: String
    }
  ],
  certficates: [
    {
      type: String
    }
  ],
  rating: [
    {
      rate: {
        type: Number
      },
      review: {
        type: String
      },
      critic: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "talent"
      }
    }
  ]
});

let ProfileModel = mongoose.model("profile", ProfileSchema);

module.exports = ProfileModel;
