const express = require("express");
const passport = require("passport")
const router = express.Router();

const Profile = require("../../models/profile")
const Interview = require("../../models/interview")
const Message = require("../../models/message")

router.get("/test", (req, res) =>
  res.json({
    msg: "seeker is working"
  })
);

router.get("/profiles", (req, res, next) => {
  Profile.find({})
  .then(profiles=>{
    res.json(profiles)
  }).catch(next);
});

router.get("/profiles/:id", (req, res, next)=>{
  Profile.findOne({_id: req.params.id})
  .then(profile=>{
    res.json(profile)
  }).catch(next)
})

router.get("/profiles/:id/rate", passport.authenticate("jwt", {session: false}), (req, res, next)=>{
  let newRating = {
    rate: req.param.rate,
    review: req.params.review,
    critic: req.user.id
  }

  Profile.findOneAndUpdate({_id: req.params.id}, {"$push": {"rating": newRating }})
  .then(profile=>{
    res.json(profile)
  }).catch(next)
})

router.post("/profiles/:id/request_interview", passport.authenticate('jwt', {session: false}), (req, res, next)=>{
  let new_interview = new Interview({
    seeker: req.user.id,
    talent: req.params.id,
    interview_date: req.body.interview_date
  })

  Interview.create(new_interview)
  .then(interview=>{
    res.json(interview)
  }).catch(next)
})

router.get("/requested_interviews", passport.authenticate("jwt", {session: false}), (req, res, next)=>{
  Interview.find({seeker: req.user.id})
  .then(interviews=>{
    res.json(interviews)
  }).catch(next)
})

router.get("/accepted_interviews", passport.authenticate("jwt", {session: false}), (req, res, next)=>{
  Interview.find({interview_accepted: true})
  .then(interviews=>{
    res.json(interviews)
  }).catch(next)
})

router.post("/profiles/:id/send_message", passport.authenticate('jwt', {session: false}), (req, res, next)=>{
  let first_message = {
    body: req.body.body,
    sender: req.user.id
  }
  let new_message = new Message({
    seeker: req.user.id,
    talent: req.params.id,
  })

  Message.create(new_message)
  .then(message=>{
    Message.findOneAndUpdate({_id: message._id}, {"$push": {"messages": first_message}})
    .then(message=>{
      res.json(message)
    }).catch(next)
  })
})

router.get("/messages", passport.authenticate("jwt", {session: false}), (req, res, next)=>{
  Message.find({seeker: req.user.id})
  .then(messages=>{
    res.json(messages)
  }).catch(next)
})


router.get("/messages/:id", passport.authenticate("jwt", {session: false}), (req, res,next)=>{
  Message.findById({_id: req.params.id})
  .then(message=>{
    res.json(message)
  }).catch(next)
})

router.get("/messages/:id/reply", passport.authenticate('jwt', {session: false}), (req, res, next)=>{
  let reply = {
    body: req.body.body,
    sender: req.user.id
  }
  Message.findOneAndUpdate({_id: req.params.id}, {"$push": {"messages": reply}})
  .then(message=>{
    res.json(message)
  }).catch(next)
})

module.exports = router;