const express = require("express");
const bcyrpt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();
const keys = require("../../config/keys");
const passport = require("passport");

//Load Talent model
const Talent = require("../../models/talent");

//@route GET api/talent/test
//@desc Tests talent route
//@access public
router.get("/test", (req, res) => res.json({ msg: "talent Works" }));

//@route POST api/talent/register
//@desc Register talent
//access Public
router.post("/register", (req, res) => {
  Talent.findOne({ email: req.body.email }).then(talent => {
    if (talent) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newTalent = new Talent({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password
      });

      //hash password using bcyrpt
      bcyrpt.genSalt(10, (err, salt) => {
        bcyrpt.hash(newTalent.password, salt, (err, hash) => {
          if (err) throw err;
          newTalent.password = hash;
          newTalent
            .save()
            .then(talent => res.json(talent))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

//@route POST api/talent/login
//@desc Login talent /Return Token
//access Public
router.post("/login", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  Talent.findOne({ email: req.body.email }).then(talent => {
    if (!talent) {
      return res.status(404).json({ email: "User not found" });
    } else {
      //Check Password
      bcyrpt.compare(password, talent.password).then(isMatch => {
        if (isMatch) {
          //User Matched

          //Create JWT Payload
          const payload = {
            id: talent.id,
            first_name: talent.first_name,
            last_name: talent.last_name
          };

          //sign Token
          jwt.sign(
            payload,
            keys.secretOrkey,
            { expiresIn: 3600 },
            (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token
              });
            }
          );
        } else {
          return res.status(400).json({ password: "Password incorrect" });
        }
      });
    }
  });
});

//@route GET api/talent/current
//@desc Returbn current talent
//access private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      first_name: req.user.first_name,
      last_name: req.user.last_name,
      email: req.user.email
    });
  }
);

module.exports = router;
