const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

// Load Profile Model
const Profile = require("./../../models/profile");

// Load Talent Model
const Talent = require("./../../models/talent");

router.get("/test", (req, res) =>
  res.json({
    msg: "Profile Works"
  })
);

//@route GET api/profile
//@desc GET Current talent Profile
//access private
router.get(
  "/",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (!profile) {
          return res.status(404).json({
            error: "There is no profile for this talent"
          });
        }
        res.json(profile);
      })
      .catch(error => console.log("[ERRORS: ]: ", error));
  }
);

//@route POST api/profile
//@desc Crete Talent Profile
//access private
router.post(
  "/",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    // GET Fields
    const profileFields = {};
    profileFields.talent = req.user.id;

    console.log("HELLO-DUDE");
    //incoming fields
    if (req.body.website) profileFields.website = req.body.website;
    if (req.body.location) profileFields.location = req.body.location;
    if (req.body.bio) profileFields.bio = req.body.bio;
    if (req.body.status) profileFields.status = req.body.status;
    if (req.body.githubusername)
      profileFields.githubusername = req.body.githubusername;

    console.log("[PROFILE_FIELDS]", profileFields);

    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["email"])
      .then(profile => {
        if (profile) {
          // Update
          Profile.findOneAndUpdate(
            {
              talent: req.user.id
            },
            {
              $set: profileFields
            },
            {
              new: true
            }
          )
            .then(profile => res.json(profile))
            .catch(error =>
              res.json({
                error: error
              })
            );
        } else {
          // Create
          new Profile(profileFields)
            .save()
            .then(profile => res.json(profile))
            .catch(error =>
              res.json({
                error: error
              })
            );
        }
      });
  }
);

//@route GET api/profile/talent/talent_id
//@desc GET talent by id
//access public
router.get("/talent/:talent_id", (req, res) => {
  Profile.findOne({
    talent: req.params.talent_id
  })
    .populate("talent", ["first_name", "email", "last_name"])
    .then(profile => {
      if (!profile) {
        return res.status(404).json({
          error: "There is no profile for this talent"
        });
      }
      res.json(profile);
    })
    .catch(error => console.log("[ERRORS: ]: ", error));
});

//@route GET api/profile/all
//@desc GET all talents
//@access public
router.get("/all", (req, res) => {
  Profile.find()
    .populate("talent", ["first_name", "last_name", "email"])
    .then(profiles => {
      if (!profiles) {
        return res.status(404).json({
          error: "There is no profile"
        });
      }
      res.json(profiles);
    })
    .catch(error => console.log("[ERRORS: ]: ", error));
});

//@route GET api/profile/search/:someParameter
//@desc GET talents
//@access public
router.get("/search/:someParameter", (req, res) => {
  console.log("[REQUEST-PARAMETER:", req.params.someParameter);
  const parameter = req.params.someParameter;
  Profile.find({
    first_name: parameter
  })
    .populate("talent", ["first_name", "last_name", "email"])
    .then(profiles => {
      if (!profiles) {
        return res.status(404).json({
          error: "There is no profile"
        });
      }
      res.json(profiles);
    })
    .catch(error => console.log("[ERRORS: ]: ", error));
});

//@route POST api/profile/experience
//@desc Add Experience
//@access private
router.post(
  "/experience",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // GET Fields
          const newExperience = {
            title: req.body.title,
            company: req.body.company,
            location: req.body.location,
            from: req.body.from,
            to: req.body.to
          };

          //add to the beginning
          profile.experience.unshift(newExperience);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route POST api/profile/education
//@desc Add education
//@access private
router.post(
  "/education",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // GET Fields
          const newEducation = {
            school: req.body.school,
            degree: req.body.degree,
            fieldofstudy: req.body.fieldofstudy,
            from: req.body.from,
            to: req.body.to
          };

          //add to the beginning
          profile.education.unshift(newEducation);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route POST api/profile/project
//@desc Add project
//@access private
router.post(
  "/project",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // GET Fields
          const newProject = {
            project_name: req.body.project_name,
            description: req.body.description,
            link: req.body.link
          };
          //display:
          console.log("[New-Project]:", newProject);

          //add to the beginning
          profile.projects.unshift(newProject);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route POST api/profile/skill
//@desc Add skill
//@access private
router.post(
  "/skill",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // GET Fields
          const newProject = {
            skill: req.body.skill,
            level: req.body.level,
            experience: req.body.experience
          };

          //add to the beginning
          profile.skills.unshift(newProject);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route DELETE api/profile/experience/:exeperience_id
//@desc Delete Experience
//@access private
router.delete(
  "/experience/:experience_id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // Get the remove index
          const removeIndex = profile.experience
            .map(item => item.id)
            .indexOf(req.params.experience_id);
          //splice from the array
          profile.experience.splice(removeIndex, 1);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route DELETE api/profile/education/:education_id
//@desc Delete education
//@access private
router.delete(
  "/education/:education_id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // Get the remove index
          const removeIndex = profile.education
            .map(item => item.id)
            .indexOf(req.params.education_id);
          //splice from the array
          profile.education.splice(removeIndex, 1);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route DELETE api/profile/projects/:project
//@desc Delete project
//@access private
router.delete(
  "/project/:project_id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // Get the remove index
          const removeIndex = profile.projects
            .map(item => item.id)
            .indexOf(req.params.project_id);
          //splice from the array
          profile.projects.splice(removeIndex, 1);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route Update api/profile/skill/:id
//@desc UPdate skill
//@access private
router.post(
  "/skills/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res, next) => {
    Profile.findOneAndUpdate(
      {
        talent: req.user.id,
        skills: { $elemMatch: { _id: req.params.id } }
      },
      {
        $set: {
          "skills.$.skill": req.body.skill,
          "skills.$.level": req.body.level,
          "skills.$.experience": req.body.experience
        }
      }
    )
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        res.json(profile);
      })
      .catch(next);
  }
);

//@route DELETE api/profile/skill/:skill_id
//@desc Delete skill
//@access private
router.delete(
  "/skill/:skill_id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // Get the remove index
          const removeIndex = profile.skills
            .map(item => item.id)
            .indexOf(req.params.skill_id);

          //splice from the array
          profile.skills.splice(removeIndex, 1);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);

//@route Update api/profile/experience/:id
//@desc UPdate skill
//@access private
router.post(
  "/experience/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res, next) => {
    Profile.findOneAndUpdate(
      {
        talent: req.user.id,
        experience: { $elemMatch: { _id: req.params.id } }
      },
      {
        $set: {
          "experience.$.title": req.body.title,
          "experience.$.company": req.body.company,
          "experience.$.location": req.body.location,
          "experience.$.from": req.body.from,
          "experience.$.to": req.body.to
        }
      }
    )
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        res.json(profile);
      })
      .catch(next);
  }
);

//@route Update api/profile/projects/:id
//@desc UPdate projects
//@access private
router.post(
  "/projects/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res, next) => {
    Profile.findOneAndUpdate(
      {
        talent: req.user.id,
        projects: { $elemMatch: { _id: req.params.id } }
      },
      {
        $set: {
          "projects.$.project_name": req.body.project_name,
          "projects.$.description": req.body.description,
          "projects.$.link": req.body.link
        }
      }
    )
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        res.json(profile);
      })
      .catch(next);
  }
);

//@route Update api/profile/education/:id
//@desc UPdate education
//@access private
router.post(
  "/education/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res, next) => {
    Profile.findOneAndUpdate(
      {
        talent: req.user.id,
        education: { $elemMatch: { _id: req.params.id } }
      },
      {
        $set: {
          "education.$.school": req.body.school,
          "education.$.degree": req.body.degree,
          "education.$.fieldofstudy": req.body.fieldofstudy,
          "education.$.from": req.body.from,
          "education.$.to": req.body.to
        }
      }
    )
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        res.json(profile);
      })
      .catch(next);
  }
);

module.exports = router;

//@route POST api/profile/picture/:id
//@desc Add Picture
//@access private
router.post(
  "/image/:id",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    Profile.findOne({
      talent: req.user.id
    })
      .populate("talent", ["first_name", "email"])
      .then(profile => {
        if (profile) {
          // GET Fields
          req//add to the beginning
          .profile.experience
            .unshift(newExperience);

          profile.save().then(profile => res.json(profile));
        }
      });
  }
);
