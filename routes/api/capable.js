const router = require("express").Router()
const passport = require("passport")

const Message = require("../../models/message")
const Interview = require("../../models/interview")

// get all messages sent to the logged in talent
router.get('/messages', passport.authenticate('jwt', {
    session: false
}), (req, res, next) => {
    Message.find({
            talent: req.user.id
        })
        .then(messages => {
            res.json(messages)
        }).catch(next)
})

// get a message
router.get("/messages/:id", passport.authenticate("jwt", {
    session: false
}), (req, res, next) => {
    Message.findOne({
            _id: req.params.id
        })
        .then(message => {
            res.json(message)
        }).catch(next)
})

// reply to message
router.post("/messages/:id", passport.authenticate("jwt", {
    session: false
}), (req, res, next) => {
    let reply = {
        body: req.body.body,
        sender: req.user.id
    }

    Message.findOneAndUpdate({
            _id: req.params.id
        }, {
            "$push": {
                "messages": reply
            }
        })
        .then(message => res.json(message))
        .catch(next)
})

// get all interview requests
router.get('/requests', passport.authenticate('jwt', {session: false}), (req, res, next)=>{
    Interview.find({talent: req.user.id})
    .then(requests=>res.json(requests))
    .catch(next)
})

// read a request
router.get("/requests/:id", passport.authenticate('jwt', {session: false}), (req, res, next)=>{
    Interview.findOne({_id: req.params.id})
    .then(request=>res.json(request))
    .catch(next)
})

// accept a request
router.put("/requests/:id", passport.authenticate('jwt', {session: false}), (req, res, next)=>{
    Interview.findOneAndUpdate({_id: req.params.id}, {interview_accepted: true})
    .then(request=>res.json(request))
    .catch(next)
})

module.exports = router