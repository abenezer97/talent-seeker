const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const mongoose = require("mongoose");

const Talent = mongoose.model("talent");
const Keys = require("./keys");

const options = {};
options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
options.secretOrKey = Keys.secretOrkey;

module.exports = passport => {
  passport.use(
    new JwtStrategy(options, (jwt_payload, done) => {
      Talent.findById(jwt_payload.id)
        .then(talent => {
          if (talent) {
            return done(null, talent);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
};
