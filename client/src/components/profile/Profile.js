import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getProfileById } from "./../../actions/profileActions";
import Progress from "../common/spinner/Progress";
import Header from "./Header";
import Portfolio from "./Portfolio";
import Git from "./Git";
import Reviews from "./Reviews";
class Profile extends Component {
  componentDidMount() {
    console.log("COMPONENT DID MOUNT");
    if (this.props.match.params.id) {
      this.props.getProfileById(this.props.match.params.id);
    }
  }
  render() {
    const { profile, loading } = this.props.profile;
    console.log("[profile]-", profile);
    let profileComponent;
    if (profile === null || loading) {
      profileComponent = <Progress />;
    } else {
      if (Object.keys(profile).length > 0) {
        profileComponent = (
          <React.Fragment>
            <Header profile={profile} />
            <Portfolio profile={profile} />
          </React.Fragment>
        );
      } else {
        profileComponent = <div>No Profile For this user</div>;
      }
    }
    return <div>{profileComponent}</div>;
  }
}

Profile.prototypes = {
  getProfileById: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  mapStateToProps,
  { getProfileById }
)(Profile);
