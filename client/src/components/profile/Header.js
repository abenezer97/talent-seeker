import React, { Component } from "react";
import isEmpty from "./../../utils/isEmpty";
import MailIcon from "@material-ui/icons/Mail";
import PhoneIcon from "@material-ui/icons/Phone";
import UserImage from "../common/image/user.png";

class Header extends Component {
  render() {
    const { profile } = this.props;
    return (
      <div className="row">
        <div className="col-md-12">
          {/* <div className="card card-body bg-light text-white mb-3"> */}
          <div className="jumbotron my-4 text-dark">
            <div className="row">
              <div className="col-4 col-md-3 m-auto">
                <img
                  className="rounded-circle"
                  src={UserImage}
                  alt=""
                  style={{ width: "100px", margin: "auto", display: "block" }}
                />
              </div>
            </div>
            <div className="text-center">
              <h1 className="display-4 text-center">
                {profile.talent.first_name} {profile.talent.last_name}
              </h1>
              <p className="lead text-center">{profile.status} </p>
              <p className="lead text-center">
                {isEmpty(profile.bio) ? null : <span>{profile.bio}</span>}
              </p>
              {isEmpty(profile.location) ? null : <p>{profile.location}</p>}
              <p>
                <a
                  className="text-dark p-2"
                  href={`http://${profile.talent.email}`}
                  target="_blank"
                >
                  <MailIcon /> {profile.talent.email}
                </a>
                {/* <i className="fas fa-phone fa-1x" /> {profile.website} */}
                {isEmpty(profile.website) ? null : (
                  <a
                    className="text-dark p-2"
                    href={`http://${profile.website}`}
                    target="_blank"
                  >
                    <i className="fas fa-globe fa-1x" /> {profile.website}
                  </a>
                )}
                <a
                  className="text-dark p-2"
                  href={profile.githubusername}
                  target="_blank"
                >
                  <i className="fab fa-github fa-1x" /> {profile.githubusername}
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
