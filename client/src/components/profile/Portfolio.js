import React, { Component } from "react";
import isEmpty from "./../../utils/isEmpty";
import Moment from "react-moment";

class Portfolio extends Component {
  render() {
    const { profile } = this.props;
    let experience;
    let education;
    let projects;
    let skill;

    //Fetching Experience
    if (profile.experience.length > 0) {
      const exp = profile.experience.map(ex => (
        <tr key={ex._id}>
          <td>{ex.company}</td>
          <td>{ex.title}</td>
          <td>{ex.location}</td>
          <td>
            <Moment format="MM-YYYY">{ex.from}</Moment>
          </td>
          <td>
            <Moment format="MM-YYYY">{ex.to}</Moment>
          </td>
        </tr>
      ));

      experience = (
        <div className="row">
          <div className="col-md-12">
            <div className="card card-body text-white mb-3">
              <div className="row">
                {/* Experience Table */}
                <table className="table table-striped table-hover btn-info">
                  <caption className="">Experience</caption>
                  <thead>
                    <tr>
                      <th scope="col">Company</th>
                      <th scope="col">role</th>
                      <th scope="col">location</th>
                      <th scope="col">from</th>
                      <th scope="col">to</th>
                    </tr>
                  </thead>
                  <tbody>{exp}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      experience = null;
    }

    if (profile.education.length > 0) {
      const edu = profile.education.map(ex => (
        <tr key={ex._id}>
          <td>{ex.school}</td>
          <td>{ex.degree}</td>
          <td>{ex.fieldofstudy}</td>
          <td>
            <Moment format="YYYY/MM/DD">{ex.from}</Moment>
          </td>
          <td>
            <Moment format="YYYY/MM/DD">{ex.to}</Moment>
          </td>
        </tr>
      ));
      education = (
        <div className="row">
          <div className="col-md-12">
            <div className="card card-body text-white mb-3">
              <div className="row">
                {/* Education Table */}
                <table className="table table-striped table-hover btn-info">
                  <caption className="">Education</caption>
                  <thead>
                    <tr>
                      <th scope="col">school</th>
                      <th scope="col">degree</th>
                      <th scope="col">field of study</th>
                      <th scope="col">from</th>
                      <th scope="col">to</th>
                    </tr>
                  </thead>
                  <tbody>{edu}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      education = null;
    }
    if (profile.projects.length > 0) {
      const pro = profile.projects.map(project => (
        <tr key={project._id}>
          <td>{project.project_name}</td>
          <td>{project.description}</td>
          <td>
            {" "}
            <a href={`http://${project.link}`} className="btn-info">
              {project.link}
            </a>
          </td>
        </tr>
      ));
      projects = (
        <div className="row">
          <div className="col-md-12">
            <div className="card card-body text-white mb-3">
              <div className="row">
                {/* Project Table */}
                <table className="table table-striped table-hover btn-info">
                  <caption className="">Projects</caption>
                  <thead>
                    <tr>
                      <th scope="col">Project</th>
                      <th scope="col">Description</th>
                      <th scope="col">Link</th>
                    </tr>
                  </thead>
                  <tbody>{pro}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      projects = null;
    }

    //Fetching Skills
    if (profile.skills.length > 0) {
      const skills = profile.skills.map(skill => (
        <tr key={skill._id}>
          <td>{skill.skill}</td>
          <td>{skill.level}</td>
          <td>{skill.experience}</td>
        </tr>
      ));

      skill = (
        <div className="row">
          <div className="col-md-12">
            <div className="card card-body text-white mb-3">
              <div className="row">
                {/* Skill Table */}
                <table className="table table-striped table-hover btn-info">
                  <caption className="">Skills</caption>
                  <thead>
                    <tr>
                      <th scope="col">Skill</th>
                      <th scope="col">Level</th>
                      <th scope="col">Experience</th>
                    </tr>
                  </thead>
                  <tbody>{skills}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      skill = null;
    }
    return (
      <div>
        {experience}
        {education}
        {projects}
        {skill}
      </div>
    );
  }
}

export default Portfolio;
