import React from "react";
import ProgressImage from "./progress.gif";

export default function Progress() {
  return (
    <div>
      <img
        src={ProgressImage}
        alt="Loading"
        style={{ width: "200px", margin: "auto", display: "block" }}
      />
    </div>
  );
}
