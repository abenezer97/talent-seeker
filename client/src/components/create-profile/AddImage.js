import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { addImage } from "../../actions/profileActions";

class AddImage extends Component {
  render() {
    return (
      <div className="row">
        <p>Image Upload</p>
        <form onSubmit={this.onSubmit}>
          <input type="file" name="image" />
          <br />
          <br />
          <button type="submit" name="upload">
            Upload
          </button>
        </form>
      </div>
    );
  }
}

const MapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  MapStateToProps,
  { addImage }
)(withRouter(AddImage));
