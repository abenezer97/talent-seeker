import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { createProfile } from "../../actions/profileActions";

class CreateProfile extends Component {
  constructor() {
    super();
    this.state = {
      bio: "",
      status: "",
      location: "",
      website: "",
      githubusername: "",
      errors: {}
    };
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const profile = {
      bio: this.state.bio,
      status: this.state.status,
      location: this.state.location,
      website: this.state.website,
      githubusername: this.state.githubusername
    };
    // call create-profile action
    this.props.createProfile(profile, this.props.history);
  };
  render() {
    const { errors } = this.state;

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row jumbotron mt-5">
            <div className="col-md-8 m-auto">
              <h1 className="display-4">Create-Your-Profile</h1>
              <p className="lead ">Add Some Profile</p>
              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-sm w-50", {
                      "is-invalid": errors.name
                    })}
                    placeholder="bio"
                    name="bio"
                    value={this.state.bio}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-sm w-50", {
                      "is-invalid": errors.name
                    })}
                    placeholder="status"
                    name="status"
                    value={this.state.status}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-sm w-50", {
                      "is-invalid": errors.location
                    })}
                    placeholder="location "
                    name="location"
                    value={this.state.location}
                    onChange={this.onChange}
                  />
                  {errors.location && (
                    <div className="invalid-feedback">{errors.location}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-sm w-50", {
                      "is-invalid": errors.website
                    })}
                    placeholder="website"
                    name="website"
                    value={this.state.website}
                    onChange={this.onChange}
                  />
                  {errors.website && (
                    <div className="invalid-feedback">{errors.website}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-sm w-50", {
                      "is-invalid": errors.githubusername
                    })}
                    placeholder=" githubusername"
                    name="githubusername"
                    value={this.state.githubusername}
                    onChange={this.onChange}
                  />
                  {errors.githubusername && (
                    <div className="invalid-feedback">
                      {errors.githubusername}
                    </div>
                  )}
                </div>
                <input
                  type="submit"
                  className="btn btn-info btn-block mt-4 w-50"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateProfile.prototypes = {
  createProfile: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  mapStateToProps,
  { createProfile }
)(withRouter(CreateProfile));
