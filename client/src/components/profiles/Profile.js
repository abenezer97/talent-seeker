import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import UserImage from "../common/image/user.png";

class Profile extends Component {
  render() {
    const { profile } = this.props;
    return (
      <div className="col-lg-3 col-md-6 mb-4">
        <div className="card h-100">
          <img
            className="rounded-circle img-dark"
            src={UserImage}
            alt=""
            style={{ width: "100px", margin: "auto", display: "block" }}
          />
          <div className="card-body">
            <p className="lead">
              {profile.talent.first_name} {profile.talent.last_name}
            </p>
            <p className="text-muted">{profile.status}</p>
            <p className="text-muted">{profile.location}</p>
          </div>
          <div className="card-footer m-auto">
            <Link
              to={`/profile/${profile.talent._id}`}
              className="btn btn-primary"
            >
              Find Out More!
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

Profile.prototypes = {
  profile: PropTypes.object.isRequired
};

export default Profile;
