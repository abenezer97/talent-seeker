import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getProfiles } from "./../../actions/profileActions";
import Progress from "./../common/spinner/Progress";
import Profile from "./Profile";
import Search from "./Search";

class Profiles extends Component {
  componentDidMount() {
    this.props.getProfiles();
  }
  render() {
    const { profiles, loading } = this.props.profile;
    let profileItems;
    if (profiles === null || loading) {
      profileItems = <Progress />;
    } else {
      if (profiles.length > 0) {
        profileItems = profiles.map(profile => (
          <Profile key={profile._id} profile={profile} />
        ));
      } else {
        profileItems = <h1>No profiles</h1>;
      }
    }
    return (
      <div>
        <Search profiles={profiles} />
        <div className="row">{profileItems}</div>
      </div>
    );
  }
}

Profiles.prototypes = {
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  getProfiles: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  mapStateToProps,
  { getProfiles }
)(Profiles);
