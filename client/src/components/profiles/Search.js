import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { searchProfiles } from "../../actions/profileActions";

class Search extends Component {
  state = {
    search: ""
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const request = {
      search: this.state.search
    };

    console.log(request.search, this.props.profiles);
    //search by first name
    const profiles = this.props.profiles.filter(
      profile =>
        profile.talent.first_name
          .toLowerCase()
          .includes(request.search.toLowerCase()) ||
        profile.talent.last_name
          .toLowerCase()
          .includes(request.search.toLowerCase()) ||
        profile.talent.email
          .toLowerCase()
          .includes(request.search.toLowerCase()) ||
        profile.location.toLowerCase().includes(request.search.toLowerCase())
    );
    if (profiles) {
      console.log("[PROFILEs-Searched]", profiles);
      this.props.searchProfiles(profiles);
    }
  };
  render() {
    return (
      <div className="row">
        <div className="col-lg-4 col-md-6 mb-4">
          <form onSubmit={this.onSubmit} className="form-inline active-cyan-4">
            <input
              className="form-control form-control-sm mr-3 w-100"
              type="text"
              placeholder="Search"
              aria-label="Search"
              name="search"
              value={this.state.search}
              onChange={this.onChange}
            />
            {/* <i className="fas fa-search" aria-hidden="true" /> */}
          </form>
        </div>
      </div>
    );
  }
}

Search.prototypes = {
  searchProfiles: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  mapStateToProps,
  { searchProfiles }
)(Search);
