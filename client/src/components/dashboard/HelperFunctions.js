import React from "react";
import { Link } from "react-router-dom";

function HelperFunctions() {
  return (
    <div className="row">
      <Link to="/update-profile" style={{ margin: "10px" }}>
        <p className="lead">Edit Profile</p>
      </Link>
      <Link to="/add-experience" style={{ margin: "10px" }}>
        <p className="lead"> Add Experience</p>
      </Link>
      <Link to="/add-education" style={{ margin: "10px" }}>
        <p className="lead"> Add Education</p>
      </Link>
      <Link to="/add-skill" style={{ margin: "10px" }}>
        <p className="lead"> Add Skill</p>
      </Link>
      <Link to="/add-project" style={{ margin: "10px" }}>
        <p className="lead"> Add Project</p>
      </Link>
      {/* <Link to="/add-image" style={{ margin: "10px" }}>
        <p className="lead"> Add Image</p>
      </Link> */}
    </div>
  );
}

export default HelperFunctions;
