import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteSkill } from "../../actions/profileActions";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

class DisplaySkill extends Component {
  handleDelete = skill_id => {
    this.props.deleteSkill(skill_id);
  };
  render() {
    if (this.props.skill.length > 0) {
      const skills = this.props.skill.map(sk => (
        <tr key={sk._id}>
          <td>{sk.skill}</td>
          <td>{sk.level}</td>
          <td>{sk.experience}</td>
          <td>
            <Link to={`/update-skill/${sk._id}`}>
              <IconButton size="small" color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              size="small"
              color="secondary"
              onClick={() => this.handleDelete(sk._id)}
            >
              <DeleteIcon />
            </IconButton>
          </td>
        </tr>
      ));
      return (
        <div className="row">
          <table className="table table-striped table-hover btn-primary">
            <caption className="">Skill</caption>
            <thead>
              <tr>
                <th scope="col">Skill</th>
                <th scope="col">Level</th>
                <th scope="col">Experience</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>{skills}</tbody>
          </table>
        </div>
      );
    } else {
      return null;
    }
  }
}

DisplaySkill.prototypes = {
  deleteSkill: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteSkill }
)(DisplaySkill);
