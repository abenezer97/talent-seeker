import React, { Component } from "react";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteExperience } from "../../actions/profileActions";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

class DisplayExperience extends Component {
  handleDelete = experience_id => {
    this.props.deleteExperience(experience_id);
  };
  render() {
    if (this.props.experience.length > 0) {
      const exp = this.props.experience.map(ex => (
        <tr key={ex._id}>
          <td>{ex.company}</td>
          <td>{ex.title}</td>
          <td>{ex.location}</td>
          <td>
            <Moment format="MM-YYYY">{ex.from}</Moment>
          </td>
          <td>
            <Moment format="MM-YYYY">{ex.to}</Moment>
          </td>
          <td>
            <Link to={`/update-experience/${ex._id}`}>
              <IconButton size="small" color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              size="small"
              color="secondary"
              onClick={() => this.handleDelete(ex._id)}
            >
              <DeleteIcon />
            </IconButton>
          </td>
        </tr>
      ));
      return (
        <div className="row">
          <table className="table table-striped table-hover btn-info">
            <caption className="">Experience</caption>
            <thead>
              <tr>
                <th scope="col">Company</th>
                <th scope="col">role</th>
                <th scope="col">location</th>
                <th scope="col">from</th>
                <th scope="col">to</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>{exp}</tbody>
          </table>
        </div>
      );
    } else {
      return null;
    }
  }
}

DisplayExperience.prototypes = {
  deleteExperience: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteExperience }
)(DisplayExperience);
