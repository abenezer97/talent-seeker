import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteProject } from "../../actions/profileActions";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

class DisplayProject extends Component {
  handleDelete = project_id => {
    this.props.deleteProject(project_id);
  };
  render() {
    if (this.props.project.length > 0) {
      const projects = this.props.project.map(pro => (
        <tr key={pro._id}>
          <td>{pro.project_name}</td>
          <td>{pro.description}</td>
          <td>
            <a href={`http://${pro.link}`} className="btn-info">
              {pro.link}
            </a>
          </td>
          <td>
            <Link to={`/update-project/${pro._id}`}>
              <IconButton size="small" color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              size="small"
              color="secondary"
              onClick={() => this.handleDelete(pro._id)}
            >
              <DeleteIcon />
            </IconButton>
          </td>
        </tr>
      ));
      return (
        <div className="row">
          <table className="table table-striped table-hover btn-info">
            <caption className="">Projects</caption>
            <thead>
              <tr>
                <th scope="col">Project</th>
                <th scope="col">Description</th>
                <th scope="col">Link</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>{projects}</tbody>
          </table>
        </div>
      );
    } else {
      return null;
    }
  }
}

DisplayProject.prototypes = {
  deleteProject: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteProject }
)(DisplayProject);
