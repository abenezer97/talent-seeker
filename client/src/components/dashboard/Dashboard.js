import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getCurrentProfile } from "../../actions/profileActions";
import Progress from "../common/spinner/Progress";
import HelperFunctions from "./HelperFunctions";
import DisplayExperience from "./DisplayExperience";
import DisplayEducation from "./DisplayEducation";
import DisplayProject from "./DisplayProject";
import DisplaySkill from "./DisplaySkill";

class Dashboard extends Component {
  componentDidMount() {
    this.props.getCurrentProfile();
  }
  render() {
    const { talent } = this.props.auth;
    const { profile, loading } = this.props.profile;
    let dashboardContent;

    if (profile === null || loading) {
      dashboardContent = <Progress />;
    } else {
      if (Object.keys(profile).length > 0) {
        dashboardContent = (
          <div>
            <p className="display-4 lead text-muted">
              <Link to={`/profile/${profile.talent._id}`}>
                {talent.first_name}
              </Link>{" "}
              {talent.last_name}
            </p>
            <div className="container">
              <HelperFunctions />
            </div>
            <div className="container">
              <DisplayExperience experience={profile.experience} />
            </div>
            <div className="container">
              <DisplayEducation education={profile.education} />
            </div>
            <div className="container">
              <DisplayProject project={profile.projects} />
            </div>
            <div className="container">
              <DisplaySkill skill={profile.skills} />
            </div>
          </div>
        );
      } else {
        dashboardContent = (
          <div>
            <p className="lead text-muted">Hi {talent.first_name}</p>
            <Link to="/create-profile" className="btn btn-lg btn-info">
              Create Profile
            </Link>
          </div>
        );
      }
    }

    return (
      <div className="dashboard">
        <div className="container">
          <div className="row">
            <div className="col-md-12">{dashboardContent}</div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.prototypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile }
)(Dashboard);
