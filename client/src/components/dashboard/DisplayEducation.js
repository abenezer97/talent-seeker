import React, { Component } from "react";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteEducation } from "../../actions/profileActions";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

class DisplayEducation extends Component {
  handleDelete = education_id => {
    this.props.deleteEducation(education_id);
  };
  render() {
    if (this.props.education.length > 0) {
      const edu = this.props.education.map(edu => (
        <tr key={edu._id}>
          <td>{edu.school}</td>
          <td>{edu.degree}</td>
          <td>{edu.fieldofstudy}</td>
          <td>
            <Moment format="MM-YYYY">{edu.from}</Moment>
          </td>
          <td>
            <Moment format="MM-YYYY">{edu.to}</Moment>
          </td>
          <td>
            <Link to={`/update-education/${edu._id}`}>
              <IconButton size="small" color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              size="small"
              color="secondary"
              onClick={() => this.handleDelete(edu._id)}
            >
              <DeleteIcon />
            </IconButton>
          </td>
        </tr>
      ));
      return (
        <div className="row">
          <table className="table table-striped table-hover btn-primary">
            <caption className="">Education</caption>
            <thead>
              <tr>
                <th scope="col">school</th>
                <th scope="col">degree</th>
                <th scope="col">field of study</th>
                <th scope="col">from</th>
                <th scope="col">to</th>
                <th scope="col">actions</th>
              </tr>
            </thead>
            <tbody>{edu}</tbody>
          </table>
        </div>
      );
    } else {
      return null;
    }
  }
}
DisplayEducation.prototypes = {
  deleteEducation: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteEducation }
)(DisplayEducation);
