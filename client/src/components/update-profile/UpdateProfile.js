import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { createProfile, getCurrentProfile } from "../../actions/profileActions";
import isEmpty from "../../utils/isEmpty";

class CreateProfile extends Component {
  constructor() {
    super();
    this.state = {
      bio: "",
      status: "",
      location: "",
      website: "",
      githubusername: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      //set empty fields to ""
      profile.bio = !isEmpty(profile.bio) ? profile.bio : "";
      profile.status = !isEmpty(profile.status) ? profile.status : "";
      profile.location = !isEmpty(profile.location) ? profile.location : "";
      profile.website = !isEmpty(profile.website) ? profile.website : "";
      profile.githubusername = !isEmpty(profile.githubusername)
        ? profile.githubusername
        : "";

      //set State
      this.setState({
        bio: profile.bio,
        status: profile.status,
        location: profile.location,
        website: profile.website,
        githubusername: profile.githubusername
      });
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const profile = {
      bio: this.state.bio,
      status: this.state.status,
      location: this.state.location,
      website: this.state.website,
      githubusername: this.state.githubusername
    };
    
    // call update-profile action
    this.props.createProfile(profile, this.props.history);
  };
  render() {
    const { errors } = this.state;

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Update-Your-Profile</h1>
              <p className="lead text-center">Add Some Profile</p>
              <p className="lead text-muted">* = required</p>
              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="bio"
                    name="bio"
                    value={this.state.bio}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="status"
                    name="status"
                    value={this.state.status}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.location
                    })}
                    placeholder="location "
                    name="location"
                    value={this.state.location}
                    onChange={this.onChange}
                  />
                  {errors.location && (
                    <div className="invalid-feedback">{errors.location}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.website
                    })}
                    placeholder="website"
                    name="website"
                    value={this.state.website}
                    onChange={this.onChange}
                  />
                  {errors.website && (
                    <div className="invalid-feedback">{errors.website}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.githubusername
                    })}
                    placeholder=" githubusername"
                    name="githubusername"
                    value={this.state.githubusername}
                    onChange={this.onChange}
                  />
                  {errors.githubusername && (
                    <div className="invalid-feedback">
                      {errors.githubusername}
                    </div>
                  )}
                </div>
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateProfile.prototypes = {
  createProfile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  mapStateToProps,
  { createProfile, getCurrentProfile }
)(withRouter(CreateProfile));
