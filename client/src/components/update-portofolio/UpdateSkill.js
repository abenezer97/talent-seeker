import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { updateSkill, getCurrentProfile } from "../../actions/profileActions";
import isEmpty from "../../utils/isEmpty";

class UpdateSkill extends Component {
  constructor() {
    super();
    this.state = {
      skill: "",
      level: "",
      experience: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      //set empty fields to ""
      profile.skills.map(skill => {
        if (skill._id === this.props.match.params.skill_id) {
          //set State
          this.setState({
            skill: skill.skill,
            level: skill.level,
            experience: skill.experience,
            profile: profile
          });
        }
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newSkill = {
      skill: this.state.skill,
      level: this.state.level,
      experience: this.state.experience
    };
    // call update-profile action
    this.props.updateSkill(
      this.props.match.params.skill_id,
      newSkill,
      this.props.history
    );
  };
  render() {
    const { errors } = this.state;

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Update-Your-Skill</h1>
              <p className="lead text-muted">* = required</p>
              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="skill"
                    name="skill"
                    value={this.state.skill}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="level"
                    name="level"
                    value={this.state.level}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.location
                    })}
                    placeholder="experience "
                    name="experience"
                    value={this.state.experience}
                    onChange={this.onChange}
                  />
                  {errors.location && (
                    <div className="invalid-feedback">{errors.location}</div>
                  )}
                </div>
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateSkill.prototypes = {
  updateSkill: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const MapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  MapStateToProps,
  { updateSkill, getCurrentProfile }
)(withRouter(UpdateSkill));
