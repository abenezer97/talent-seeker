import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import {
  updateEducation,
  getCurrentProfile
} from "../../actions/profileActions";

class UpdateEducation extends Component {
  constructor() {
    super();
    this.state = {
      school: "",
      degree: "",
      fieldofstudy: "",
      from: "",
      to: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      //set empty fields to ""
      profile.education.map(edu => {
        if (edu._id === this.props.match.params.education_id) {
          //set State
          this.setState({
            school: edu.school,
            degree: edu.degree,
            fieldofstudy: edu.fieldofstudy,
            from: edu.from,
            to: edu.to
          });
        }
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newEducation = {
      school: this.state.school,
      degree: this.state.degree,
      fieldofstudy: this.state.fieldofstudy,
      from: this.state.from,
      to: this.state.to
    };
    const profile = this.props.profile.profile;

    // call update-profile action
    this.props.updateEducation(
      this.props.match.params.education_id,
      newEducation,
      this.props.history
    );
  };

  render() {
    const { errors } = this.state;

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Update-Your-Education</h1>
              <p className="lead text-muted">* = required</p>
              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="school"
                    name="school"
                    value={this.state.school}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="qualification"
                    name="degree"
                    value={this.state.degree}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.fieldofstudy
                    })}
                    placeholder="fieldofstudy "
                    name="fieldofstudy"
                    value={this.state.fieldofstudy}
                    onChange={this.onChange}
                  />
                  {errors.fieldofstudy && (
                    <div className="invalid-feedback">
                      {errors.fieldofstudy}
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="date"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.from
                    })}
                    placeholder="from"
                    name="from"
                    value={this.state.from}
                    onChange={this.onChange}
                  />
                  {errors.from && (
                    <div className="invalid-feedback">{errors.from}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="date"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.to
                    })}
                    placeholder=" to"
                    name="to"
                    value={this.state.to}
                    onChange={this.onChange}
                  />
                  {errors.to && (
                    <div className="invalid-feedback">{errors.to}</div>
                  )}
                </div>
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateEducation.prototypes = {
  updateEducation: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const MapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  MapStateToProps,
  { updateEducation, getCurrentProfile }
)(withRouter(UpdateEducation));
