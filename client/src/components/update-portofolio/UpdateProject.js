import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { updateProject, getCurrentProfile } from "../../actions/profileActions";

class UpdateProject extends Component {
  constructor() {
    super();
    this.state = {
      project_name: "",
      description: "",
      link: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      //set empty fields to ""
      profile.projects.map(project => {
        if (project._id === this.props.match.params.project_id) {
          //set State
          this.setState({
            project_name: project.project_name,
            description: project.description,
            link: project.link
          });
        }
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newProject = {
      project_name: this.state.project_name,
      description: this.state.description,
      link: this.state.link
    };
    const profile = this.props.profile.profile;

    const newProjects = profile.projects.map(pro => {
      if (pro._id === this.props.match.params.project_id) {
        pro = {
          ...pro,
          project_name: newProject.project_name,
          description: newProject.description,
          link: newProject.link
        };
      }
      return pro;
    });

    const newProfile = { ...profile, projects: newProjects };
    console.log("[NEW--Profile]", newProfile);

    // call update-profile action
    // call update-profile action
    this.props.updateProject(
      this.props.match.params.project_id,
      newProject,
      this.props.history
    );
  };

  render() {
    const { errors } = this.state;

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Update-Your-Project</h1>
              <p className="lead text-muted">* = required</p>
              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="project_name"
                    name="project_name"
                    value={this.state.project_name}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.name
                    })}
                    placeholder="projet description"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.location
                    })}
                    placeholder="link "
                    name="link"
                    value={this.state.link}
                    onChange={this.onChange}
                  />
                  {errors.location && (
                    <div className="invalid-feedback">{errors.location}</div>
                  )}
                </div>
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateProject.prototypes = {
  updateProject: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const MapStateToProps = state => ({
  profile: state.profile,
  error: state.error
});

export default connect(
  MapStateToProps,
  { updateProject, getCurrentProfile }
)(withRouter(UpdateProject));
