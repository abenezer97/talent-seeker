import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { clearCurrentProfile } from "../../actions/profileActions";

export class Navbar extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  };

  render() {
    const { isAuthenticated, talent } = this.props.auth;

    const authLinks = (
      <div className="navbar-nav ml-auto">
        <Link to="/dashboard" className="nav-item nav-link">
          Dashboard
        </Link>
        <a href="/" className="nav-link" onClick={this.onLogoutClick}>
          Logout
        </a>
      </div>
    );

    const guestLinks = (
      <div className="navbar-nav ml-auto">
        <Link to="/login" className="nav-item nav-link">
          Login
        </Link>
        <Link to="/register" className="nav-item nav-link">
          Register
        </Link>
      </div>
    );

    return (
      <nav className="navbar navbar-expand-md navbar-light bg-light">
        <Link to="/" className="navbar-brand">
          <img
            src="/examples/images/logo.svg"
            height="28"
            alt="Talent-Seek &trade;"
          />
        </Link>
        <button
          type="button"
          className="navbar-toggler"
          data-toggle="collapse"
          data-target="#navbarCollapse"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarCollapse">
          <div className="navbar-nav ml-auto">
            <Link to="/" className="nav-item nav-link active">
              Home
            </Link>
            <Link to="/about" className="nav-item nav-link">
              About
            </Link>
          </div>
          {isAuthenticated ? authLinks : guestLinks}
        </div>
      </nav>
    );
  }
}

Navbar.prototypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  clearCurrentProfile: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser, clearCurrentProfile }
)(Navbar);
