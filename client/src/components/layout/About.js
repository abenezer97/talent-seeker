import React, { Component } from "react";

class About extends Component {
  render() {
    return (
      <div>
        <header className="jumbotron my-4">
          <h1 className="display-3">About</h1>
          <p className="lead text-mute">
            This WebSite is Developed as a first project by Abenezer S. and
            Natnael M. when they have been working as an intern developer in
            ModernEth.
          </p>
        </header>
      </div>
    );
  }
}

export default About;
