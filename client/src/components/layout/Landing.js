import React, { Component } from "react";
import Profiles from "../profiles/Profiles";

export class Landing extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <header className="jumbotron my-4">
            <h1 className="display-3">Welcome To Talent-Seek&trade; </h1>
            <p className="lead">
              Talent-Seek&trade; is a platform provided by{" "}
              <a href="http://www.moderneth.com/">ModernETH</a> to bridg the gap
              between recruiters and tech proffessionals. Using this platform
              tech professionals will be able to create their portofolio along
              with important credintials so that it can be accessed by
              recruiters.
            </p>
            <a href="/here" className="btn btn-primary btn-lg">
              Learn More about our services
            </a>
          </header>
          <Profiles />
        </div>
      </React.Fragment>
    );
  }
}

export default Landing;
