import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };
  }
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log("[You Dd it Man.]");
    if (nextProps.auth.isAuthenticated) {
      console.log("[You Did it Man.]");
      this.props.history.push("/dashboard");
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const talent = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(talent);
  };

  render() {
    return (
      <div className="login">
        <div className="container">
          <div className="row jumbotron mt-5">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 ">Log In</h1>
              <p className="lead ">Login to your Talent-Seek&trade; account</p>
              <form onSubmit={this.onSubmit} className="col-md-8 m-auto">
                <div className="form-group">
                  <input
                    type="email"
                    className="form-control form-control-sm w-50"
                    placeholder="Email Address"
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className="form-control form-control-sm w-50"
                    placeholder="Password"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                </div>
                <input
                  type="submit"
                  className="btn btn-info btn-block mt-4 w-50"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.prototypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.error
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
