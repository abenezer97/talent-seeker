import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

import jwt_dcode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import Navbar from "./components/layout/Navbar";
import Landing from "./components/layout/Landing";
import Register from "./talent/auth/Register";
import Login from "./talent/auth/Login";
import Footer from "./components/layout/Footer";
import Dashboard from "./components/dashboard/Dashboard";
// import "./App.css";
import { clearCurrentProfile, addSkill } from "./actions/profileActions";
import PrivateRoute from "./components/common/PrivateRoute";
import CreateProfile from "./components/create-profile/CreateProfile";
import UpdateProfile from "./components/update-profile/UpdateProfile";
import AddExperience from "./components/add-portofolio/AddExperience";
import AddEducation from "./components/add-portofolio/AddEducation";
import Profile from "./components/profile/Profile";
import AddProject from "./components/add-portofolio/AddProject";
import AddSkill from "./components/add-portofolio/AddSkill";
import UpdateSkill from "./components/update-portofolio/UpdateSkill";
import UpdateProject from "./components/update-portofolio/UpdateProject";
import UpdateEducation from "./components/update-portofolio/UpdateEducation";
import UpdateExperience from "./components/update-portofolio/UpdateExperience";
import AddImage from "./components/create-profile/AddImage";
import About from "./components/layout/About";

//Check for token
if (localStorage.jwtToken) {
  //set auth token header auth
  setAuthToken(localStorage.jwtToken);
  //decode token and get user info and exp
  const decoded = jwt_dcode(localStorage.jwtToken);
  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  //Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout the user
    store.dispatch(logoutUser());
    //clear Current Profile
    store.dispatch(clearCurrentProfile());

    //TODO: Clear Cureent Profile
    //Redirect to login
    window.location.href = "/login";
  }
}

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navbar />
          <div className="container" style={{ minHeight: "90vh" }}>
            <Route exact path="/" component={Landing} />
            <div className="container">
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/profile/:id" component={Profile} />
              <Route exact path="/about" component={About} />
              <Switch>
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/create-profile"
                  component={CreateProfile}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/update-profile"
                  component={UpdateProfile}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/add-experience"
                  component={AddExperience}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/add-education"
                  component={AddEducation}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/add-project"
                  component={AddProject}
                />
              </Switch>
              <Switch>
                <PrivateRoute exact path="/add-skill" component={AddSkill} />
              </Switch>
              <Switch>
                <PrivateRoute exact path="/add-image" component={AddImage} />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/update-experience/:experience_id"
                  component={UpdateExperience}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/update-education/:education_id"
                  component={UpdateEducation}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/update-project/:project_id"
                  component={UpdateProject}
                />
              </Switch>
              <Switch>
                <PrivateRoute
                  exact
                  path="/update-skill/:skill_id"
                  component={UpdateSkill}
                />
              </Switch>
            </div>
          </div>

          <Footer />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
