import axios from "axios";
import jwt_decode from "jwt-decode";
import { GET_ERRORS, SET_CURRENT_USER } from "./types";
import setAuthToken from "../utils/setAuthToken";

//Register User
export const registerTalent = (talentData, history) => dispatch => {
  axios
    .post("/api/talent/register", talentData)
    .then(res => history.push("/login"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login

export const loginUser = talentData => dispatch => {
  axios
    .post("/api/talent/login", talentData)
    .then(res => {
      //save to localStorage
      const { token } = res.data;
      //set token to localStorage
      localStorage.setItem("jwtToken", token);
      //set token to Auth header
      setAuthToken(token);
      //decode token to get talentData
      const decoded = jwt_decode(token);
      //set Current User
      dispatch(setCurrentUser(decoded));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setCurrentUser = talentData => {
  return {
    type: SET_CURRENT_USER,
    payload: talentData
  };
};

export const logoutUser = () => dispatch => {
  //remove token from local storage
  localStorage.removeItem("jwtToken");
  //remove Authorization header for future request
  setAuthToken(false);
  //set current user {}
  dispatch(setCurrentUser({}));
};
