import axios from "axios";

import {
  GET_PROFILE,
  GET_PROFILES,
  PROFILE_LOADING,
  GET_ERRORS,
  CLEAR_CURRENT_PROFILE
} from "./types";

// Get current profile
export const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get("/api/profile")
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PROFILE,
        payload: {}
      })
    );
};

// Get profile by id
export const getProfileById = talent_id => dispatch => {
  console.log("start get-profile-by-id");
  dispatch(setProfileLoading());
  axios
    .get(`/api/profile/talent/${talent_id}`)
    .then(res => {
      console.log("Processing FETCH PROFILE--");
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
      console.log("Processed FETCH PROFILE--");
    })
    .catch(err => {
      console.log("Faced An ERROR");
      dispatch({
        type: GET_PROFILE,
        payload: {}
      });
    });
};

// Get profiles
export const getProfiles = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get("/api/profile/all")
    .then(res =>
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PROFILES,
        payload: {}
      })
    );
};

// Search profiles
export const searchProfiles = profiles => dispatch => {
  dispatch(setProfileLoading());
  dispatch({
    type: GET_PROFILES,
    payload: profiles
  });
};

// Create Profile
export const createProfile = (profileData, history) => dispatch => {
  console.log("[DOING-SOME-STUFF]");
  axios
    .post("api/profile", profileData)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[ERROR-BRO]");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Add Experience
export const addExperience = (newExperiece, history) => dispatch => {
  axios
    .post("api/profile/experience", newExperiece)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Experience
export const deleteExperience = experiece_id => dispatch => {
  axios
    .delete(`api/profile/experience/${experiece_id}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Education
export const addEducation = (newEducation, history) => dispatch => {
  axios
    .post("api/profile/education", newEducation)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Education
export const deleteEducation = education_id => dispatch => {
  axios
    .delete(`api/profile/education/${education_id}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Project
export const addProject = (newProject, history) => dispatch => {
  axios
    .post("api/profile/project", newProject)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Project
export const deleteProject = project_id => dispatch => {
  axios
    .delete(`api/profile/project/${project_id}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Skill
export const addSkill = (newSkill, history) => dispatch => {
  axios
    .post("api/profile/skill", newSkill)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Update Skill
export const updateSkill = (id, newSkill, history) => dispatch => {
  axios
    .post(`/api/profile/skills/${id}`, newSkill)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[Got-sOme-Erros");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Add Image
export const addImage = (id, newImage, history) => dispatch => {
  axios
    .post(`/api/profile/image/${id}`, newImage)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[Got-sOme-Erros");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Update Project
export const updateProject = (id, newProject, history) => dispatch => {
  axios
    .post(`/api/profile/projects/${id}`, newProject)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[Got-sOme-Erros");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Update Experience
export const updateExperience = (id, newExperiece, history) => dispatch => {
  axios
    .post(`/api/profile/experience/${id}`, newExperiece)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[Got-sOme-Erros");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Update Education
export const updateEducation = (id, newEducation, history) => dispatch => {
  axios
    .post(`/api/profile/education/${id}`, newEducation)
    .then(res => history.push("/dashboard"))
    .catch(err => {
      console.log("[Got-sOme-Erros");
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Delete Skill
export const deleteSkill = skill_id => dispatch => {
  axios
    .delete(`api/profile/skill/${skill_id}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Profile loading
export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING
  };
};

// Clear Profile
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  };
};
